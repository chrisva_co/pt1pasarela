<?php
return [
    'OK' => 'Solicitud exitosa',
    'FAIL_INVALID_AUTH' => 'Datos de autenticación inválidos',
    'FAIL_INVALID_SEED' => 'Validez de la petición ha expirado',
    'FAIL_MAINTENANCE' => 'Servicio no disponible por mantenimiento',
    'FAIL_NO_DATA' => 'No hay información coincidente con los datos proporcionados',
    'FAIL_BILL_CANCELED' => 'La factura u orden de pago ya está cancelada',
    'FAIL_BILL_EXPIRED' => 'La factura u orden de pago ya está vencida',
    'FAIL_BILL_NOT_EXIST' => 'La factura u orden de pago a ser asentada no existe',
    'FAIL_INVALID_VALUE' => 'Valor del asiento de pago es inválido o no corresponde al
valor de la factura u orden de pago',
    'FAIL_INCONSIENT_VALUE' => 'El valor del asiento no equivale a la suma del monto en
efectivo y en cheque.',
    'FAIL_INVALID_FRANCHISE' => 'Franquicia o entidad de recaudadora inválida',
    'FAIL_INVALID_METHOD' => 'Método de recaudo inválido',
    'FAIL_INVALID_CHANNEL' => 'Canal de recaudo inválido',
    'FAIL_INVALID_SETTLE' => 'Asiento inválido (alguna validación de datos no fue exitosa)',
    'FAIL_UNEXPECTED' => 'Fallo inesperado en el servicio'
];
