<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use DateTime;

class Factura extends Model
{
    //protected $fillable = ['user_id', 'cantante_id', 'titulo', 'album','genero', 'letra','video','anoPublicacion','activo'];

    //es para usar ->format() ya que por una extraña razon no funciono en makeda, en melissa si
    public function getDate(){
        return new DateTime($this->created_at);
    }

    public function cliente(){
        return $this->belongsTo('App\Cliente', 'cliente_debtorID', 'debtorID');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function pagos(){
        return $this->hasMany('App\Pago', 'factura_reference', 'reference');
    }

    public static function toPasarela($facturas, $enJson=false){
        $response = function($factura){
            return [
                'reference'=>$factura->reference,
                'description'=>$factura->description,
                'debtorID'=>$factura->cliente->debtorID,
                'debtorFirstName'=>$factura->cliente->debtorFirstName,
                'debtorLastName'=>$factura->cliente->debtorLastName,
                'debtorCode'=>$factura->debtorCode,
                'totalCharges'=>$factura->totalCharges,
                'totalInterest'=>$factura->totalInterest,
                'totalAmount'=>$factura->totalAmount,
                'creationDate'=>$factura->created_at,
            ];
        };

        $result = [];
        foreach ($facturas as $factura) {
            $result[] = $response($factura);
        }
        $result = self::response('OK', $result);

        if ($enJson){
            return $result;
        }
    }

    public static function response(string $codigo, $billInfo)
    {
        return [
            'status' => $codigo,
            'description' => config('constants.'.$codigo),
            'bills' => $billInfo
        ];
    }
}
