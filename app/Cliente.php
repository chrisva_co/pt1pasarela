<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use DateTime;

class Cliente extends Model
{
    //protected $fillable = ['user_id', 'cantante_id', 'titulo', 'album','genero', 'letra','video','anoPublicacion','activo'];

    //es para usar ->format() ya que por una extraña razon no funciono en makeda, en melissa si
    public function getDate(){
        return new DateTime($this->created_at);
    }

    public function facturas(){
        return $this->hasMany('App\Factura', 'cliente_debtorID', 'debtorID');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
