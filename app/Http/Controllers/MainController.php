<?php
/*
Relaciones y LISTAS! https://stackoverflow.com/questions/16210628/laravel-relationships
*/
namespace App\Http\Controllers;

use App\Cliente;
use App\Factura;
use App\Pago;
use \Config;
use Illuminate\Http\Request;

class MainController extends Controller{

    private $request;

    public function __construct()
    {
        $this->request = (new Request())->all();
    }


    public function soap(){
        try {
            $server = new \SoapServer(
                null
                ,['uri'=> 'http://'.$_SERVER['HTTP_HOST'].'/soap']
                //'classmap' => array('clase' => "MiClase")
            );
            $server->setClass('App\Http\Controllers\MainController');
            $server->handle();

        } catch (\SOAPFault $f) {
            print $f->faultstring;
        }
    }

    public function getBillByReference(){
        $params = func_get_args()[0];
        $this->validarDatos('getBillByReference', $params);

        $facturas = Factura::where('reference', $params->reference)->get();
        return Factura::toPasarela($facturas, true);
    }

    public function getBillByDebtorID(Request $request){
        $params = func_get_args()[0];
        $this->validarDatos('getBillByDebtorID', $params);

        $facturas = Cliente::where('debtorID', $params->debtorID)->first()->facturas;
        return Factura::toPasarela($facturas, true);
    }

    public function getBillByDebtorCode(){
        $params = func_get_args()[0];
        $this->validarDatos('getBillByDebtorCode', $params);

        $facturas = Factura::where('debtorCode', $params->debtorCode)->get();
        return Factura::toPasarela($facturas, true);
    }

    public function settlePayment(){
        $params = func_get_args()[0];
        $this->validarDatos('settlePayment', $params);
        $datosDelPago = [
            'reference'=>'223344',
            'totalAmount'=>'48000',
            'date'=>date('Y-m-d'),
            'receipt'=>'',
            'franchise'=>'12S345',
            'channel'=>'OFC',
            'method'=>'CASH',
            'cashAmount'=>'50000',
            'payerID'=>'22231145',
            'agreement'=>'12345',
            'agentID'=>'codigoCajero',
            'location'=>'codigoOficina',
        ];

        $rules = array(
            'titulo' => 'required|min:1|max:180',
            'letra' => 'required|min:100',
        );

        $pago = new Pago($params);
        if ($pago->save()){
            return $pago->response('OK');
        }
    }

    public function validarDatos($servicio, $request){
        return 1;
    }
}
