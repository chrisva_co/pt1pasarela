<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return "view('welcome');";
});


Route::post('soap', 'MainController@soap');
Route::get('soap', 'MainController@soap');

Route::post('getBillByReference', 'MainController@getBillByReference');
Route::post('getBillByDebtorID', 'MainController@getBillByDebtorID');
Route::post('getBillByDebtorCode', 'MainController@getBillByDebtorCode');
