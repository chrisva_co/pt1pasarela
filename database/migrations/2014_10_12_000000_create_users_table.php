<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('debtorFirstName', 80);
            $table->string('debtorLastName', 80);
            $table->string('debtorID', 45)->unique();
            $table->timestamps();
        });

        Schema::create('facturas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference', 45)->unique();
            $table->string('description', 300)->nullable();
            $table->string('debtorCode', 45);
            $table->double('totalCharges')->nullable();
            $table->double('totalInterest')->nullable();
            $table->string('cliente_debtorID', 45);
            $table->foreign('cliente_debtorID')->references('debtorID')->on('clientes');
            $table->timestamps();
        });

        Schema::create('pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->double('totalAmount')->nullable();
            $table->dateTime('date');
            $table->string('receipt', 45)->nullable();
            $table->string('franchise', 5);
            $table->string('channel', 3);
            $table->string('method', 5)->nullable()->default('CASH');
            $table->double('cashAmount')->nullable();
            $table->string('playerID', 45)->nullable();
            $table->string('agreement', 10)->nullable();
            $table->string('agentID', 20)->nullable();
            $table->string('location', 20)->nullable();

            $table->string('factura_reference', 45);
            $table->foreign('factura_reference')->references('reference')->on('facturas');
            $table->timestamps();
        });

/*
        Schema::create('config', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 255);
            $table->string('descripcion', 5000);
            $table->boolean('activo')->default(true);
            $table->timestamps();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('clientes');
        Schema::dropIfExists('facturas');
        Schema::dropIfExists('pagos');
    }
}
